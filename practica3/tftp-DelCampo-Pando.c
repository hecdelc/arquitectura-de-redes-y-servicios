// Practica tema 7, Del Campo Pando Hector

#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netdb.h>
#include <unistd.h>

#define TRUE 1
#define FALSE 0
#define RANDOM_PORT 0
#define SERVICE "tftp"
#define PROTOCOL "udp"
#define PROTOCOL_TYPE 0
#define DATA_LENGTH 516 // 512 de datos, 4 de header
#define IP_LENGTH 15
#define ACK_LENGTH 4
#define FLAGS 0
#define SUCCESS 0
#define USAGE_MSG "Uso: IP {-r|-w} archivo [-v]\n"

#define CODE_RRQ 0x0001
#define CODE_WRQ 0x0002
#define CODE_DATA 0x0003
#define CODE_ACK 0x0004
#define CODE_ERROR 0x0005
#define CODE_LENGTH 2
#define NUM_ERRORES 8
#define WRITE_ACCEPT 0

char *errors[NUM_ERRORES] = {"No definido",
     		"Fichero no encontrado",
		"Violacion de acceso",
		"Espacio de almacenamiento lleno",
		"Operacion TFTP ilegal",
		"Identificador de transferencia desconocido",
		"El fichero ya existe",
		"Usuario desconocido"};

#define TFTP_MODE "octet"
#define MODE_ARG_LENGTH 2
#define READ 0
#define WRITE 1
#define ERROR_MODE -1

int verbose;
int mode = ERROR_MODE;
FILE *fich;
unsigned short last_block;

int createSocket();
short int getDefaultPort();
void bindSocket(const int sock, const struct sockaddr_in *my_addr);
void sendRequest(const int sock, struct sockaddr_in *serv_addr, const char *path, const char *ip);
unsigned int sendData(const int sock, struct sockaddr_in *serv_addr, const char *path);
char* prepareData(const char *path);
int recieveData(const int sock, struct sockaddr_in *serv_addr, const char *dir);
void writeFile (const char *dir, const char *buff, const int num_elementos);
int checkServerError (char *buff);
unsigned short recieveACK(const int sock, struct sockaddr_in *serv_addr);
void sendACK(const int sock, struct sockaddr_in *serv_addr, const unsigned short num_block);
void prepareClientAddr(struct sockaddr_in *addr);
void prepareServerAddr(struct sockaddr_in *addr, const unsigned int ip); //Que la IP sea unsigned o no, en realidad no afecta, ya que no vamos a realizar operaciones.

int main(int numArg, char* arg[]) {
	
	struct in_addr addr;
	verbose = FALSE;
	last_block = 1;

	// opterr = 0 para evitar que sea el sistema  quien muestre mensaje de error, habra que mostrar con '?' el formato
	// c representa el argumento opcional, de haberlo
	int c;
	opterr = 0;

	while (-1 != (c = getopt(numArg,  arg, "rwv"))) {
		switch(c) {
			case 'v':
				verbose = TRUE;
				break;
			case 'w':
				if (ERROR_MODE != mode) {
					printf(USAGE_MSG);
					return EXIT_FAILURE;
				}
				mode = WRITE;
				break;
			case 'r':
				if (ERROR_MODE != mode) {
					printf(USAGE_MSG);
					return EXIT_FAILURE;
				}
				mode = READ;
				break;
			case '?':
				printf(USAGE_MSG);
				return EXIT_FAILURE;
			default:
				abort();
		}
	}


	// A lo sumo 4 argumentos, minimo 3, y el modo puesto
	if ((numArg - optind) > 2 || numArg < 4 || ERROR_MODE == mode) {
		printf(USAGE_MSG);
		return EXIT_FAILURE;
	}

	// La IP es obligatoria, asi que la transformamos a unsigned int en network byte order
	char *ip = malloc(sizeof(char) * strlen(arg[optind]));
	strcpy(ip, arg[optind]);
	inet_aton(ip, &addr);

	optind++;

	// El fichero tambien es obligatorio
	char *path_fich = malloc(sizeof(char) * strlen(arg[optind]));
	strcpy(path_fich, arg[optind]);

	const int sock = createSocket();

	// Estructura con informacion del cliente, utilizada para la apertura del socket
	struct sockaddr_in my_addr;
	prepareClientAddr(&my_addr);

	bindSocket(sock, &my_addr);

	// Estructura con informacion del servidor, utilizada tanto en recepcion como en envio
	struct sockaddr_in serv_addr;
	prepareServerAddr(&serv_addr, addr.s_addr);

	// Enviamos peticion, y empezamos a recibir datos
	sendRequest(sock, &serv_addr, path_fich, ip);
	
	if (WRITE == mode) {
		if (WRITE_ACCEPT == recieveACK(sock, &serv_addr)){
			if(verbose) {
				printf("Empezando envio de fichero\n");
			}
			while((DATA_LENGTH-4) == sendData(sock, &serv_addr, path_fich));
		}
	} else {
		while((DATA_LENGTH-4) == recieveData(sock, &serv_addr, path_fich));
	}

	if (verbose) {
		printf("El bloque #%hu era el ultimo, cerrando\n", last_block - 1);
	}

	close(sock);

	free(ip);
	free(path_fich);
	
	return EXIT_SUCCESS;

}

int createSocket() {

	// socket IPv4 UDP generico
	int sock = socket(AF_INET,
			SOCK_DGRAM,
			PROTOCOL_TYPE);

	if (sock < SUCCESS) {
		perror("socket()");
		exit(EXIT_FAILURE);
	}

	return sock;

}

void bindSocket(const int sock, const struct sockaddr_in *my_addr) {

	const int error = bind(sock,
			(struct sockaddr *) my_addr,
			sizeof(*my_addr));

	if (error < SUCCESS) {
		perror("bind()");
		exit(EXIT_FAILURE);
	}

}

void sendRequest(const int sock, struct sockaddr_in *serv_addr, const char *path, const char *ip) {

	// Enviamos una solicitud de lectura / escritura segun corresponda, respetando el protocolo
	unsigned int size = sizeof(char) * (strlen(path) + strlen(TFTP_MODE) + CODE_LENGTH*2);
	char *msg = malloc(size);
	memset(msg, 0, size);

	// Los codigos han de ser transformados a network byte order
	unsigned short correct_num;

	switch (mode) {
		case WRITE:
			correct_num = htons(CODE_WRQ);
			break;
		case READ:
			correct_num = htons(CODE_RRQ);
			break;
		default:
			correct_num = ERROR_MODE;
			break;
	}

	// Ponemos el codigo, el path, y el modo
	memcpy(msg, &correct_num, 2);
	strcpy(&msg[CODE_LENGTH], path);
	msg[strlen(path) + CODE_LENGTH] = 0;
	strcpy(&msg[strlen(path) + 1 + CODE_LENGTH], TFTP_MODE);

	const int error = sendto(sock, 
			msg,
			size,
			FLAGS,
			(struct sockaddr *) serv_addr,
			sizeof(*serv_addr));

	if (error < SUCCESS) {
		perror("Sending request to server");
		exit(EXIT_FAILURE);
	}

	free(msg);

	if (verbose) {
		printf("Enviada solicitud de");

		switch (mode) {
			case WRITE:
				printf(" escritura ");
				break;
			case READ:
				printf(" lectura ");
				break;
			default:
				printf(" desconocido ");
				break;
		}

		printf("de %s a servidor tftp en %s\n", path, ip);
	}

}

unsigned int sendData(const int sock, struct sockaddr_in *serv_addr, const char *path) {

	// Preparamos los datos, los enviamos, comprobamos ACK y posibles errores del servidor
	char *data = prepareData(path);

	const unsigned int bytes = strlen(data);
	const unsigned int msg_length =	sizeof(char) * (2*CODE_LENGTH + bytes); 

	char *buff = malloc(msg_length);

	// Copiamos codigo
	unsigned short code;
	code = htons(CODE_DATA);
	memcpy (buff, &code, CODE_LENGTH);

	// Copiamos #paquete
	unsigned short num_block;
	num_block = htons(last_block);
	memcpy (&buff[2], &num_block, CODE_LENGTH);

	// Copiamos contenido
	memcpy (&buff[4], data, msg_length);
	
	const int error = sendto(sock, 
			buff,
			msg_length,
			FLAGS,
			(struct sockaddr *) serv_addr,
			sizeof(*serv_addr));

	if (error < SUCCESS) {
		perror("Sending data to server");
		exit(EXIT_FAILURE);
	}

	if (verbose) {
		printf("Enviado el bloque #%hu de %u bytes \n", last_block, bytes);
	}

	if (recieveACK(sock, serv_addr) == last_block) {
		last_block++;
	}

	free(buff);
	free(data);

	return bytes;

}

char* prepareData(const char *path) {

	// Abrimos fichero, ponemos el puntero en la posicion correspondiente, y vamos llenando el buffer
	fich = fopen(path, "r");

	if (NULL == fich) {
		perror("Opening file");
		exit(EXIT_FAILURE);
	}

	char *buff = malloc(sizeof(char) * (DATA_LENGTH-4));
	memset(buff, 0, DATA_LENGTH - 4);

	fseek(fich, (last_block-1)*512, SEEK_SET);

	int i;
	char ch;
	for (i=0; i<DATA_LENGTH-4; i++){
		ch = fgetc(fich);
		if (EOF == ch) {
			break;
		}

		buff[i] = ch;
	}

	const int error = fclose(fich);

	if (SUCCESS != error) {
		perror("Closing file");
		exit(EXIT_FAILURE);
	}

	return buff;
}

unsigned short recieveACK(const int sock, struct sockaddr_in *serv_addr) {

	// Esperamos a la recepcion del ACK, y devolvemos el #paquete confirmado
	socklen_t msg_leng = sizeof(struct sockaddr_in);

	char buff[DATA_LENGTH];

	const int error = recvfrom (sock,
			buff,
			DATA_LENGTH,
			FLAGS,
			(struct sockaddr *) serv_addr,
			&msg_leng);

	if (error < SUCCESS) {
		perror("Error reciving ACK");
		exit(EXIT_FAILURE);
	}

	if (TRUE == checkServerError(buff)) {
		exit(EXIT_FAILURE);
	}

	unsigned short numACK;
	memcpy(&numACK, &buff[2], CODE_LENGTH);
	numACK = ntohs(numACK);

	if(verbose) {
		printf("Recibido ACK #%hu\n", numACK);
	}

	return numACK;
}

int recieveData(const int sock, struct sockaddr_in *serv_addr, const char *dir) {

	// Recibimos un paquete, miramos si hay algun error, sino construimos fichero
	socklen_t msg_leng = sizeof(struct sockaddr_in);

	char buff[DATA_LENGTH];

	const int num_bytes = recvfrom (sock,
			buff,
			DATA_LENGTH,
			FLAGS,
			(struct sockaddr *) serv_addr,
			&msg_leng);

	if (TRUE == checkServerError(buff)) {
		exit(EXIT_FAILURE);
	}

	if (num_bytes < SUCCESS) {
		perror("recvfrom()");
		exit(EXIT_FAILURE);
	}

	short recieved_block;
	memcpy(&recieved_block, &buff[CODE_LENGTH], CODE_LENGTH);
	recieved_block = ntohs(recieved_block);

	if (verbose) {
		printf("Recibido bloque de datos #%hu de tamano %hu\n", recieved_block, num_bytes);
	}

	// Enviamos el ACK del ultimo paquete recibido en orden
	if (recieved_block == last_block) {
		writeFile(dir, &buff[CODE_LENGTH*2], num_bytes - CODE_LENGTH*2);
		sendACK(sock, serv_addr, last_block);
		last_block++;
	} else {
		sendACK(sock, serv_addr, last_block);
	}


	return num_bytes - CODE_LENGTH*2;

}

void writeFile(const char *dir, const char *buff, const int num_elementos) {

	// Escritura a fichero
	fich = fopen(dir, "a");

	if (NULL == fich) {
		perror("Opening file");
		exit(EXIT_FAILURE);
	}

	fwrite(buff, sizeof(char), num_elementos, fich);

	if (SUCCESS != ferror(fich)) {
		perror("Writing file");
		exit(EXIT_FAILURE);
	}

	const int error = fclose(fich);

	if (SUCCESS != error) {
		perror("Closing file");
		exit(EXIT_FAILURE);
	}

}

int checkServerError(char *buff) {

	// Comprueba si hay algun error y lo muestra, la funcion en si no trata el error
	unsigned short code;
	memcpy(&code, buff, CODE_LENGTH);
	code = ntohs(code);

	if (CODE_ERROR == code) {
		unsigned short error_num;
		memcpy(&error_num, &buff[CODE_LENGTH], CODE_LENGTH);
		error_num = ntohs(error_num);

		printf("Error registrado: %s", errors[error_num]);

		if(0 == error_num) {
			printf("%s", &buff[CODE_LENGTH]);
		}

		printf("\n");

		return TRUE;
	}

	return FALSE;

}

void sendACK(const int sock, struct sockaddr_in *serv_addr, const unsigned short num_block) {

	char msg[ACK_LENGTH];

	// Los codigos, y numero de bloque han de ser transformados a network byte order
	const unsigned short code = htons(CODE_ACK);
	memcpy(msg, &code, CODE_LENGTH);

	const unsigned short block = htons(num_block);
	memcpy(&msg[CODE_LENGTH], &block, CODE_LENGTH);

	// Enviamos el ACK al servidor del ultimo bloque secuencial recibido
	const int error = sendto(sock, 
			msg,
			ACK_LENGTH,
			FLAGS,
			(struct sockaddr *) serv_addr,
			sizeof(*serv_addr));

	if (error < SUCCESS) {
		perror("sendto() ACK");
		exit(EXIT_FAILURE);
	}

	if (verbose) {
		printf("Enviado el ACK del bloque %hu\n", num_block);
	}

}

void prepareClientAddr(struct sockaddr_in *addr) {

	// Estructura generica IPv4, con la IP otorgada  por la API y puerto cualquiera disponible
	addr->sin_family = AF_INET;
	addr->sin_port = RANDOM_PORT;
	addr->sin_addr.s_addr = INADDR_ANY;

}

void prepareServerAddr(struct sockaddr_in *addr, const unsigned int ip) {

	// A diferencia del proceso servidor, necesitamos conocer la direccion, el puerto es el estandar
	addr->sin_family = AF_INET;
	addr->sin_port = getDefaultPort();
	addr->sin_addr.s_addr = ip;

}

short int getDefaultPort() {

	// Encuentra el puerto por defecto
	struct servent *serv;
	serv = getservbyname(SERVICE, PROTOCOL);
	if (NULL == serv) {
		printf("Error al obtener el puerto\n");
		exit(EXIT_FAILURE);
	}

	return serv->s_port;

}
