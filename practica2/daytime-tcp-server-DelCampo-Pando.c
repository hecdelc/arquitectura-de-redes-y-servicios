// Practica tema 6, Del Campo Pando Hector

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netdb.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>
#include <signal.h>

#define DAYTIME_SERVICE "daytime"
#define TCP "tcp"
#define PROTOCOL 0
#define RESPONSE_LENGTH 50 //El tamano original son 39 chars
#define MSG_LENGTH 0
#define TRUE 1
#define SUCCESS 0
#define FLAGS 0
#define BACKLOG_SIZE 5
#define NUM_THREADS BACKLOG_SIZE

sem_t sem_fich;
int serv_sock;
int threads_sock[NUM_THREADS] = {[0 ... NUM_THREADS-1] = -1 }; // -1 indica puerto libre
pthread_t threads[NUM_THREADS];
FILE *fich = NULL; // null indicara fichero cerrado

void initSem(sem_t *sem, const int num_access);
void waitForChilds();
int createSocket();
short int getStandardPort();
void listenConnections(const int sock);
void sendTime (const int sock, const char *msg);
int recieveRequest (const int sock, struct sockaddr_in *client_addr);
void bindSocket (const int sock, const struct sockaddr_in *addr);
void *respond(void *arg);
void prepareMsg (char *buff);
void prepareServerAddr(struct sockaddr_in *addr, const short int port);
void closeConnection(const int sock);
void signal_handler(const int signal);

int main(int numArg, char* arg[]) {
	
	short int port = -1;

	// opterr = 0 evitara que sea la llamada la que imprima el error, con '?' mostramos mensaje personalizado
	// c representa argumento opcional
	int c;
	opterr = 0;

	while ( -1 != (c = getopt(numArg, arg, "p:"))) {
		switch (c) {
			case 'p':
				sscanf(optarg, "%hd", &port);
				// Pasamos el entero a network byte order
				port = htons(port);
				break;
			case '?':
				printf("Uso: [-p puerto]\n");
				return EXIT_FAILURE;
			default:
				abort();
		}
	}

	// Sin mas argumentos
	if ((numArg - optind) > 0) {
		printf("Uso: [-p puerto]\n");
		return EXIT_FAILURE;
	}

	// No se ha introducido puerto, usamos el bien conocido
	if (1 == numArg) {
		port = getStandardPort();
	}

	initSem(&sem_fich, 1);


	if (SIG_ERR == signal(SIGINT, signal_handler)) {
		perror("signal()");
		exit(EXIT_FAILURE);
	}
	 
	serv_sock = createSocket();

	// Preparacion de la estructura para el servidor, tan solo se utilizara en la preparacion del puerto
	struct sockaddr_in myaddr;
	prepareServerAddr(&myaddr, port);

	bindSocket(serv_sock, &myaddr);

	// Estructura del cliente, se actualizara cada vez que se reciba una peticion, y se utilizara esta misma para el envio
	struct sockaddr_in client_addr;

	listenConnections(serv_sock);

	//Long evita warnings
	long thread_id = 0;

	//Modelo concurrente con threads
	while (TRUE) {

		// Esperamos una peticion, creamos un hilo que maneje la respuesta, y manejamos el identificador para el siguiente hilo
		threads_sock[thread_id] = recieveRequest(serv_sock, &client_addr);
		if (SUCCESS != pthread_create(threads + thread_id, NULL, respond, (void*) thread_id)) {
			printf("error al crear un nuevo thread\n");
			exit(EXIT_FAILURE);
		}

		thread_id++;

		// "Pool de threads", como mucho NUM_THREADS activos a la vez, pero siempre esperamos al ultimo thread activo antes de reiniciar la cuenta
		if (NUM_THREADS <= thread_id) {
			waitForChilds();
			thread_id = 0;
		}

	}

	return EXIT_SUCCESS;

}

void initSem(sem_t *sem, const int num_access) {

	const int error = sem_init(sem, 0, num_access);
	if (error < SUCCESS) {
		perror("sem_init()");
		exit(EXIT_FAILURE);
	}

}

int createSocket() {

	// Socket Ipv4 TCP
	const int sock = socket(AF_INET,
			SOCK_STREAM,
			PROTOCOL);

	if (sock < SUCCESS) {
		perror("socket()");
		exit(EXIT_FAILURE);
	}

	return sock;

}

short int getStandardPort() {

	struct servent *serv;
	serv = getservbyname(DAYTIME_SERVICE, TCP);
	if (NULL == serv) {
		printf("Error al obtener el puerto\n");
		exit(EXIT_FAILURE);
	}

	return serv->s_port;

}

void sendTime (const int sock, const char *msg) {

	const int error = send(sock, 
			msg,
			RESPONSE_LENGTH,
			FLAGS);

	if (error < SUCCESS) {
		perror("send()");
		exit(EXIT_FAILURE);
	}

}

int recieveRequest (const int sock, struct sockaddr_in *client_addr) {

	socklen_t msg_leng = sizeof(struct sockaddr_in);

	// Esperamos una peticion
	const int new_port = accept(sock,
			(struct sockaddr *) client_addr,
			&msg_leng);

	// Al cerrar el server, sucedera un error,
	//  no lo filtraremos para evitar no obtener un mensaje de error en caso de error real
	//  y asi facilitar la depuracion, en cualquier caso el errorno seria EBADF
	if (new_port < SUCCESS) {
		perror("accept()");
		exit(EXIT_FAILURE);
	}

	return new_port;

}

void bindSocket (const int sock, const struct sockaddr_in *addr) {

	const int error = bind(sock,
			(struct sockaddr *) addr,
			sizeof(*addr));

	if (error < SUCCESS) {
		perror("bind()");
		exit(EXIT_FAILURE);
	}

}

void prepareMsg (char *buffer) {
	
	// Cada hilo esperara a la disponibilidad del recurso, lo utilizara y lo liberara
	int error = sem_wait(&sem_fich);
	if (error < SUCCESS) {
		perror("sem_wait()");
		exit(EXIT_FAILURE);
	}

	gethostname(buffer, 8);
	buffer[8] = ':';
	buffer[9] = ' ';

	system("date > /tmp/time.txt");
	fich = fopen("/tmp/time.txt","r");

	// En caso de fallo intentamos liberar el semaforo, para que los demas hilos puedan seguir trabajando
	if (NULL == fgets(&buffer[10], RESPONSE_LENGTH - 9, fich)) {
		printf("Error en system(), en fopen() o en fgets()\n");
		error = sem_post(&sem_fich);
		if (error < SUCCESS) {
			perror("sem_post()");
		}
		exit(EXIT_FAILURE);
	}

	if (EOF == fclose(fich)) {
		perror("fclose()");
		error = sem_post(&sem_fich);
		if (error < SUCCESS) {
			perror("sem_post()");
		}
		exit(EXIT_FAILURE);
	}
	fich = NULL;

	error = sem_post(&sem_fich);
	if (error < SUCCESS) {
		perror("sem_post()");
		exit(EXIT_FAILURE);
	}

}

void prepareServerAddr(struct sockaddr_in *addr, const short int port) {

	// IPv4 generico, con el puerto seleccionado por el usuario
	addr->sin_family = AF_INET;
	addr->sin_port = port;
	addr->sin_addr.s_addr = INADDR_ANY;

}

// Handler para cada thread
void *respond(void *arg) {

	char buffer[RESPONSE_LENGTH];
	//Long para evitar warnings
	const long id = (long) arg;
	const int sock = threads_sock[id];

	prepareMsg(buffer);
	sendTime(sock, buffer);

	// un socket a -1 indica que se ha cerrado la conexion
	threads_sock[id] = -1;

	return EXIT_SUCCESS;

}

void listenConnections(const int sock) {

	const int error = listen(sock,
			BACKLOG_SIZE);

	if (error < SUCCESS) {
		perror("listen()");
		exit(EXIT_FAILURE);
	}

}

void closeConnection(const int sock) {

	int error = shutdown(sock, SHUT_RDWR);
	if (error < SUCCESS) {
		perror("shutdown()");
		exit(EXIT_FAILURE);
	}

	error = recv(sock, 
			NULL,
			MSG_LENGTH,
			FLAGS);

	if (error < SUCCESS) {
		perror("closing recv()");
		exit(EXIT_FAILURE);
	}

	// Algun cliente no esta respetando el protocolo, ya que no deberia enviarnos nada
	// aun asi cerramos el socket, aqui no ha pasado nada :D
	if (error > SUCCESS) {
		printf("Protocolo no respetado\n");
	}

	error = close(sock);
	if (error < SUCCESS) {
		perror("close()");
		exit(EXIT_FAILURE);
	}

}

void signal_handler(const int signal) {

	// Cerrado adecuado de la conexion y socket, no hace falta hacer recv de un puerto de escucha
	int error = shutdown(serv_sock, SHUT_RDWR);
	if (error < SUCCESS) {
		perror("shutdown()");
		exit(EXIT_FAILURE);
	}

	error = close(serv_sock);
	if (error < SUCCESS) {
		perror("close()");
		exit(EXIT_FAILURE);
	}

	int i;
	for (i = 0; i < NUM_THREADS; i++) {
		// Aquellos puertos libres (-1) no hay que liberarlos
		if (-1 != threads_sock[i] ) {
			closeConnection (threads_sock[i]);
		}
	}

	// Cerramos fichero en caso de que aun estéabierto
	if (NULL != fich) {
		if (EOF == fclose(fich)) {
			perror("fclose()");
			exit(EXIT_FAILURE);
		}
	}

}

void waitForChilds() {

	int i;
	// Esperamos a que terminen todos los hijos, y a su vez limpiamos memoria con join
	for (i = 0; i < NUM_THREADS; i++) {
		if (SUCCESS != pthread_join(threads[i], NULL)) {
			printf("Error en el pool");
			exit(EXIT_FAILURE);
		}
	}

}
