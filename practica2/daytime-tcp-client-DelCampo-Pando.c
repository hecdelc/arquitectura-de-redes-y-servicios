// Practica tema 6, Del Campo Pando Hector

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netdb.h>
#include <unistd.h>

#define RANDOM_PORT 0
#define DAYTIME "daytime"
#define TCP "tcp"
#define PROTOCOL 0
#define RESPONSE_LENGTH 50 //Tamano real de 39 chars
#define MSG_LENGTH 0
#define FLAGS 0
#define SUCCESS 0

int createSocket();
short int getDefaultPort();
void bindSocket(const int sock, const struct sockaddr_in *my_addr);
void connectToServer(const int sock, struct sockaddr_in *serv_addr);
void recieveTime(const int sock, char *buff);
void prepareClientAddr(struct sockaddr_in *addr);
void prepareServerAddr(struct sockaddr_in *addr, const short int port, const unsigned int ip); //Que la IP sea unsigned o no, en realidad no afecta, ya que no vamos a realizar operaciones.
void closeSocket(const int sock);

int main(int numArg, char* arg[]) {
	
	struct in_addr addr;
	short int port = -1;

	// opterr = 0 para evitar que sea el sistema  quien muestre mensaje de error, habra que mostrar con '?' el formato
	// c representa el argumento opcional, de haberlo
	int c;
	opterr = 0;

	while (-1 != (c = getopt(numArg,  arg, "p:"))) {
		switch(c) {
			case 'p':
				sscanf(optarg, "%hd", &port);
				port = htons(port);
				break;
			case '?':
				printf("Uso: IP [-p puerto]\n");
				return EXIT_FAILURE;
			default:
				abort();
		}
	}

	// A lo sumo 3 argumentos en la ejecucion & minimo ip
	if ((numArg - optind) > 1 || numArg == 1) {
		printf("Uso: IP [-p puerto]\n");
		return EXIT_FAILURE;
	}

	// La IP es obligatoria, asi que la transformamos a unsigned int en network byte order
	if ( 0 == inet_aton(arg[optind], &addr)) {
		printf("IP invalida\n");
		return EXIT_FAILURE;
	}

	// No se ha introducido argumento opcional, coger puerto por defecto DAYTIME, UDP
	if (2 == numArg) {
		port = getDefaultPort();
	}

	const int sock = createSocket();

	// Estructura con informacion del cliente, utilizada para la apertura del socket
	struct sockaddr_in my_addr;
	prepareClientAddr(&my_addr);

	bindSocket(sock, &my_addr);

	// Estructura con informacion del servidor, utilizada tanto en recepcion como en envio
	struct sockaddr_in serv_addr;
	prepareServerAddr(&serv_addr, port, addr.s_addr);

	// Nos conectamos, no enviamos nada, y recibimos la informacion daytime

	char buffer[RESPONSE_LENGTH];

	connectToServer(sock, &serv_addr);
	recieveTime(sock, buffer);

	printf("%s", buffer);

	closeSocket(sock);

	return EXIT_SUCCESS;

}

int createSocket() {

	// socket IPv4 TCP generico
	int sock = socket(AF_INET,
			SOCK_STREAM,
			PROTOCOL);

	if (sock < SUCCESS) {
		perror("socket()");
		exit(EXIT_FAILURE);
	}

	return sock;

}

void bindSocket(const int sock, const struct sockaddr_in *my_addr) {

	const int error = bind(sock,
			(struct sockaddr *) my_addr,
			sizeof(*my_addr));

	if (error < SUCCESS) {
		perror("bind()");
		exit(EXIT_FAILURE);
	}

}

void connectToServer(const int sock, struct sockaddr_in *serv_addr) {

	// Establecemos conexion
	const int error = connect(sock, 
			(struct sockaddr *) serv_addr,
			sizeof(*serv_addr));

	if (error < SUCCESS) {
		perror("connect()");
		exit(EXIT_FAILURE);
	}

}

void recieveTime(const int sock, char *buff) {

	const int error = recv(sock,
			buff,
			RESPONSE_LENGTH,
			FLAGS);

	if (error < SUCCESS) {
		perror("recv()");
		exit(EXIT_FAILURE);
	}

}

void prepareClientAddr(struct sockaddr_in *addr) {

	// Estructura generica IPv4, con la IP otorgada  por la API y puerto cualquiera disponible
	addr->sin_family = AF_INET;
	addr->sin_port = RANDOM_PORT;
	addr->sin_addr.s_addr = INADDR_ANY;

}

void prepareServerAddr(struct sockaddr_in *addr, const short int port, const unsigned int ip) {

	// A diferencia del proceso servidor, necesitamos conocer la direccion y puerto del servidor de antemano
	addr->sin_family = AF_INET;
	addr->sin_port = port;
	addr->sin_addr.s_addr = ip;

}

short int getDefaultPort() {

	struct servent *serv;
	serv = getservbyname(DAYTIME, TCP);
	if (NULL == serv) {
		printf("Error al obtener el puerto\n");
		exit(EXIT_FAILURE);
	}

	return serv->s_port;

}

void closeSocket(const int sock) {

	// Cerramos adecuadamente la conexion
	shutdown(sock, SHUT_RDWR);

	const int error = recv(sock,
			NULL,
			MSG_LENGTH,
			FLAGS);

	// No esperamos recibir nada del server
	if (error != SUCCESS) {
		perror("closing recv()");
		exit(EXIT_FAILURE);
	}

	close(sock);

}
