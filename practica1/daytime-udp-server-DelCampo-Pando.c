// Practica 1, Del Campo Pando Hector

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netdb.h>
#include <unistd.h>

#define DAYTIME_SERVICE "daytime"
#define UDP "udp"
#define PROTOCOL 0
#define RESPONSE_LENGTH 50 //El tamano original son 39 chars
#define MSG_LENGTH 0
#define TRUE 1
#define SUCCESS 0
#define FLAGS 0

int createSocket();
short int getStandardPort();
void sendTime (const int sock, struct sockaddr_in *client_addr, const char *msg);
void recieveRequest (const int sock, struct sockaddr_in *client_addr);
void bindSocket (const int sock, const struct sockaddr_in *addr);
void prepareMsg (char *buff);
void prepareServerAddr(struct sockaddr_in *addr, const short int port);

int main(int numArg, char* arg[]) {
	
	short int port = -1;

	// opterr = 0 evitara que sea la llamada la que imprima el error, con '?' mostramos mensaje personalizado
	// c representa argumento opcional
	int c;
	opterr = 0;

	while ( -1 != (c = getopt(numArg, arg, "p:"))) {
		switch (c) {
			case 'p':
				sscanf(optarg, "%hd", &port);
				// Pasamos el entero a network byte order
				port = htons(port);
				break;
			case '?':
				printf("Uso: [-p puerto]\n");
				return EXIT_FAILURE;
			default:
				abort();
		}
	}

	// Sin mas argumentos
	if ((numArg - optind) > 0) {
		printf("Uso: [-p puerto]\n");
		return EXIT_FAILURE;
	}

	// No se ha introducido puerto, usamos el bien conocido
	if (1 == numArg) {
		port = getStandardPort();
	}

	int sock = createSocket();

	// Preparacion de la estructura para el servidor, tan solo se utilizara en la preparacion del puerto
	struct sockaddr_in myaddr;
	prepareServerAddr(&myaddr, port);

	bindSocket(sock, &myaddr);

	// Estructura del cliente, se actualizara cada vez que se reciba una peticion, y se utilizara esta misma para el envio
	struct sockaddr_in client_addr;

	char buffer[RESPONSE_LENGTH];

	//Modelo iterativo
	while (TRUE) {

		// Esperamos una peticion, preparamos el mensaje y respondemos con la daytime
		recieveRequest(sock, &client_addr);
		prepareMsg(buffer);
		sendTime(sock, &client_addr, buffer);

	}

	// En realidad no hay posibilidad de cerrar el socket ni de devolver nada,
	// ya que la unica forma de salir del bucle es con una senal de terminacion al proceso.
	// En stackoverflow indican la buena practica de dejar que sea el SO quien cierre sockets, ficheros y demas abiertos
	// http://stackoverflow.com/questions/7376228/linux-c-catching-kill-signal-for-graceful-termination
	close(sock);
	
	return EXIT_SUCCESS;

}

int createSocket() {

	// Socket Ipv4 UDP
	const int sock = socket(AF_INET,
			SOCK_DGRAM,
			PROTOCOL);

	if (sock < SUCCESS) {
		perror("bind()");
		exit(EXIT_FAILURE);
	}

	return sock;

}

short int getStandardPort() {

	// Esctructura estatica, no hay que liberarla
	struct servent *serv;
	serv = getservbyname(DAYTIME_SERVICE, UDP);
	if (NULL == serv) {
		printf("Error al obtener el puerto\n");
		exit(EXIT_FAILURE);
	}

	return serv->s_port;

}

void sendTime (const int sock, struct sockaddr_in *client_addr, const char *msg) {

	const int error = sendto(sock, 
			msg,
			RESPONSE_LENGTH,
			FLAGS,
			(struct sockaddr *) client_addr,
			sizeof(*client_addr));

	if (error < SUCCESS) {
		perror("sendto()");
		exit(EXIT_FAILURE);
	}

}

void recieveRequest (const int sock, struct sockaddr_in *client_addr) {

	socklen_t msg_leng = sizeof(struct sockaddr_in);

	// No vamos a recibir nada, asi que el buffer es nulo, y el tamano 0
	const int error = recvfrom(sock,
			NULL,
			MSG_LENGTH,
			FLAGS,
			(struct sockaddr *) client_addr,
			&msg_leng);

	if (error < SUCCESS) {
		perror("recvfrom()");
		exit(EXIT_FAILURE);
	}

}

void bindSocket (const int sock, const struct sockaddr_in *addr) {

	const int error = bind(sock,
			(struct sockaddr *) addr,
			sizeof(*addr));

	if (error < SUCCESS) {
		perror("bind()");
		exit(EXIT_FAILURE);
	}

}

void prepareMsg (char *buffer) {
	
	FILE *fich;

	gethostname(buffer, 8);
	buffer[8] = ':';
	buffer[9] = ' ';

	system("date > /tmp/time.txt");
	fich = fopen("/tmp/time.txt","r");
	if (NULL == fgets(&buffer[10], RESPONSE_LENGTH - 9, fich)) {
		printf("Error en system(), en fopen() o en fgets()\n");
		exit(EXIT_FAILURE);
	}

	fclose(fich);

}

void prepareServerAddr(struct sockaddr_in *addr, const short int port) {

	// IPv4 generico, con el puerto seleccionado por el usuario
	addr->sin_family = AF_INET;
	addr->sin_port = port;
	addr->sin_addr.s_addr = INADDR_ANY;

}
