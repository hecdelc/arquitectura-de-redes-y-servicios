// Practica tema 8, Del Campo Pando Hector

#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netdb.h>
#include <unistd.h>
#include "ip-icmp-ping.h"

#define TRUE 1
#define FALSE 0
#define RANDOM_PORT 0
#define FLAGS 0
#define SUCCESS 0
#define USAGE_MSG "Uso: miping IP [-v]\n"

#define HEADER_LENGTH 8
#define CHECKSUM_SIZE 2

#define PING_TYPE 0x08
#define PING_CODE 0x00

char *destination_error[] =
			{"Destination network unreachable",
			"Destination host unreachable",
			"Destination protocol unreachable",
			"Destination port unreachable",
			"Fragmentation required, and DF flag set",
			"Source route failed",
			"Destination network unknown",
			"Destination host unknown",
			"Source host isolated",
			"Network administratively prohibited",
			"Host administratively prohibited",
			"Network unreachable for ToS",
			"Host unreachable for ToS",
			"Communication administratively prohibited",
			"Host Precedence Violation",
			"Precedence cutoff in effect"};

char *time_error[] = 
		{"TTL expired in transit",
		"Fragment reassembly time exceeded"};

char *parameter_error[] =
		{"Pointer indicates the error",
		"Missing a required option",
		"Bad length"};

int verbose;

int createSocket();
void bindSocket(const int sock, const struct sockaddr_in *my_addr);
void sendICMP(const int sock, struct sockaddr_in *serv_addr, const char *ip);
void recieveICMP(const int sock, struct sockaddr_in *serv_addr);
unsigned short getChecksum(ECHORequest request);
int checkError (ICMPHeader msg);
void prepareClientAddr(struct sockaddr_in *addr);
void prepareServerAddr(struct sockaddr_in *addr, const unsigned int ip); //Que la IP sea unsigned o no, en realidad no afecta, ya que no vamos a realizar operaciones.

int main(int numArg, char* arg[]) {
	
	struct in_addr addr;
	verbose = FALSE;

	// opterr = 0 para evitar que sea el sistema  quien muestre mensaje de error, habra que mostrar con '?' el formato
	// c representa el argumento opcional, de haberlo
	int c;
	opterr = 0;

	while (-1 != (c = getopt(numArg,  arg, "v"))) {
		switch(c) {
			case 'v':
				verbose = TRUE;
				break;
			case '?':
				printf(USAGE_MSG);
				return EXIT_FAILURE;
			default:
				abort();
		}
	}


	// A lo sumo 2 argumentos, minimo 1
	if ((numArg - optind) > 1 || numArg < 2 || numArg == optind) {
		printf(USAGE_MSG);
		return EXIT_FAILURE;
	}

	// La IP es obligatoria, asi que la transformamos a unsigned int en network byte order
	char *ip = malloc(sizeof(char) * strlen(arg[optind]));
	strcpy(ip, arg[optind]);
	inet_aton(ip, &addr);

	const int sock = createSocket();

	// Estructura con informacion del cliente, utilizada para la apertura del socket
	struct sockaddr_in my_addr;
	prepareClientAddr(&my_addr);

	bindSocket(sock, &my_addr);

	// Estructura con informacion del servidor, utilizada tanto en recepcion como en envio
	struct sockaddr_in serv_addr;
	prepareServerAddr(&serv_addr, addr.s_addr);

	// Enviamos peticion, y recibimos respuesta
	sendICMP(sock, &serv_addr, ip);
	recieveICMP(sock, &serv_addr);

	close(sock);

	free(ip);
	
	return EXIT_SUCCESS;

}

int createSocket() {

	// socket IPv4 ICMP generico
	int sock = socket(AF_INET,
			SOCK_RAW,
			IPPROTO_ICMP);

	if (sock < SUCCESS) {
		perror("socket()");
		exit(EXIT_FAILURE);
	}

	return sock;

}

void bindSocket(const int sock, const struct sockaddr_in *my_addr) {

	const int error = bind(sock,
			(struct sockaddr *) my_addr,
			sizeof(*my_addr));

	if (error < SUCCESS) {
		perror("bind()");
		exit(EXIT_FAILURE);
	}

}

void sendICMP(const int sock, struct sockaddr_in *serv_addr, const char *ip) {

	// Construimos una peticion ICMP segun los codigos correspondientes para PING
	ECHORequest request;
	memset(&request, 0, sizeof(ECHORequest));

	request.icmpHeader.Type = PING_TYPE;
	request.icmpHeader.Code = PING_CODE;
	request.ID = htons(getpid());
	request.SeqNumber = htons(0);
	strcpy(request.payload, "Payload ejemplo");
	request.icmpHeader.Checksum = getChecksum(request);

	if (verbose) {
		printf("-> Generando cabecera ICMP\n");
		printf("-> Type: %hhu\n", request.icmpHeader.Type);
		printf("-> Code: %hhu\n", request.icmpHeader.Code);
		printf("-> CheckSum: %x\n", request.icmpHeader.Checksum);
		printf("-> PID: %hu\n", ntohs(request.ID));
		printf("-> Seq. number: %hu\n", ntohs(request.SeqNumber));
		printf("-> Mensaje: %s\n", request.payload);
		printf("-> Tamaño de la peticion: %lu\n", sizeof(request));
	}

	const int error = sendto(sock, 
			&request,
			sizeof(request),
			FLAGS,
			(struct sockaddr *) serv_addr,
			sizeof(*serv_addr));

	if (error < SUCCESS) {
		perror("Sending request to server");
		exit(EXIT_FAILURE);
	}

	printf("Paquete ICMP enviado a %s\n", ip);

}

unsigned short getChecksum(ECHORequest request) {

	// Calcula el checksum del mensaje de peticion segun el algoritmo visto en clase
	unsigned int checksum = 0;

	unsigned short *iterator = (unsigned short*) &request;

	int i;
	for (i=0; i<sizeof(ECHORequest) / 2; i++){
		checksum += (unsigned int) *iterator;
		iterator++;
	}

	// Suma 2 veces el acarreo para evitar posibles overflows
	checksum = (checksum >> 16) + (checksum & 0x0000ffff);
	checksum = (checksum >> 16) + (checksum & 0x0000ffff);
	checksum = ~checksum;

	return (unsigned short) checksum;

}

void recieveICMP(const int sock, struct sockaddr_in *serv_addr) {

	// Recibimos un paquete, miramos si hay algun error, sino mostramos info del ping
	socklen_t msg_leng = sizeof(struct sockaddr_in);

	ECHOResponse response;

	const int num_bytes = recvfrom (sock,
			&response,
			sizeof(response),
			FLAGS,
			(struct sockaddr *) serv_addr,
			&msg_leng);

	if (num_bytes < SUCCESS) {
		perror("recvfrom()");
		exit(EXIT_FAILURE);
	}

	printf("Respuesta recibida desde %s\n", inet_ntoa(response.ipHeader.iaSrc));

	if (verbose) {
		printf("-> Tamano de la respuesta: %u\n", num_bytes);
		printf("-> Cadena recibida: %s\n", response.payload);
		printf("-> PID: %hu\n", ntohs(response.ID));
		printf("-> TTL: %hhu\n", response.ipHeader.TTL);
	}

	if (TRUE == checkError(response.icmpHeader)) {
		exit(EXIT_FAILURE);
	}



}

int checkError(ICMPHeader msg) {

	// Comprueba si hay algun error y lo muestra, la funcion en si no trata el error
	const unsigned char type = msg.Type;
	const unsigned char code = msg.Code;

	switch (type) {
		case 0:
			switch (code) {
				case 0:
					printf("Descripcion de la respuesta correcta (tipo: %hhu, code: %hhu)\n", type, code);
					return FALSE;
				default:
					printf("Codigo de respuesta incorrecta (tipo: %hhu, code: %hhu)\n", type, code);
					return TRUE;
			}
		case 3:
			printf("Destino inalcanzable: %s (tipo: %hhu, code: %hhu)\n", destination_error[code], type, code);
			return TRUE;
		case 11:
			printf("Tiempo excedido: %s (tipo: %hhu, code: %hhu)\n", time_error[code], type, code);
			return TRUE;
		case 12:
			printf("Cabecera IP incorrecta: %s (tipo: %hhu, code: %hhu)\n", parameter_error[code], type, code);
			return TRUE;
		default:
			printf("Recibido tipo desconocido\n");
			return TRUE;
	}

}

void prepareClientAddr(struct sockaddr_in *addr) {

	// Estructura generica IPv4, con la IP otorgada  por la API y puerto cualquiera disponible
	addr->sin_family = AF_INET;
	addr->sin_port = RANDOM_PORT;
	addr->sin_addr.s_addr = INADDR_ANY;

}

void prepareServerAddr(struct sockaddr_in *addr, const unsigned int ip) {

	// A diferencia del proceso servidor, necesitamos conocer la direccion,
	// el puerto, ahora puede ser cualquiera, ya que la peticion no llegara a la capa superior
	addr->sin_family = AF_INET;
	addr->sin_port = RANDOM_PORT;
	addr->sin_addr.s_addr = ip;

}